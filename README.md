# F18 MFDs

This project outlines the design files for creating a set of MFDs to use with DCS-World from the Thrustmaster Cougar MFD button pads and some small LCD displays.

Each display has the same front/rear enclosure parts, then a bolt pattern on the rear of the enclosure exposes how a mount can be added (ie wall mount, table mount, etc). Mounts for the left/right MFDs provide horizontal rotation and some tilt. The lower MFD mount was designed to bolt onto the two rods used in the Winwing F18 stick.

The LCD this was designed for is Pimoroni Ltd PIM372 8" HDMI display.

## Solidworks

Contains the 3D models of the enclosures. The model includes the 3d printed parts as well as threaded inserts and fasteners (all part numbers are for McMaster-Carr).

## Datasheets

Contains the limited but useful documentation for the lcd panels.
